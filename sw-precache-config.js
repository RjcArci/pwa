module.exports = {
    navigateFallBack:'index.html',
    stripPrefix: '',
    root:'',
  staticFileGlobs: [
    'styles/**.css',
    '**.html',
    'images/**.*',
    'scripts/**.js'
  ]
};